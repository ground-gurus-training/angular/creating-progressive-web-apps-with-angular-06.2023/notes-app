import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';

import { NoteDetailsComponent } from './note-details.component';
import { HeaderComponent } from 'src/app/layouts/header/header.component';
import { FooterComponent } from 'src/app/layouts/footer/footer.component';

@NgModule({
  declarations: [NoteDetailsComponent],
  imports: [
    HeaderComponent,
    FooterComponent,
    InputTextModule,
    InputTextareaModule,
    ButtonModule,
    ToastModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  exports: [NoteDetailsComponent],
})
export class NoteDetailsModule {}
