import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { timer } from 'rxjs';
import { Note } from 'src/app/model/note';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-note-details',
  templateUrl: './note-details.component.html',
  styleUrls: ['./note-details.component.scss'],
})
export class NoteDetailsComponent implements OnInit {
  isNew: boolean = false;
  id: string = '';
  noteForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
  });

  constructor(
    private noteService: NoteService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe(async (params) => {
      const id = params['id'];

      if (id === 'new') {
        this.isNew = true;
      }

      const doc = await this.noteService.getNote(id);

      if (doc.exists()) {
        const data = doc.data();

        this.id = doc.id;
        this.noteForm.patchValue({
          title: data['title'],
          content: data['content'],
        });
      }
    });
  }

  save() {
    if (this.isNew) {
      this.noteService.addNote({
        title: this.noteForm.get('title')?.value,
        content: this.noteForm.get('content')?.value,
      });
    } else {
      this.noteService.saveNote({
        id: this.id,
        title: this.noteForm.get('title')?.value,
        content: this.noteForm.get('content')?.value,
      } as Note);
    }

    this.messageService.add({
      summary: 'Save',
      detail: 'Save Successful.',
    });

    if (this.isNew) {
      timer(1000).subscribe(() => {
        this.back();
      });
    }
  }

  back() {
    this.router.navigateByUrl('');
  }
}
