import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { Observable } from 'rxjs';
import { Note } from 'src/app/model/note';
import { NoteService } from 'src/app/services/note.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  notes: Note[] = [];

  constructor(
    private noteService: NoteService,
    private confirmationService: ConfirmationService,
    private router: Router
  ) {}

  ngOnInit() {
    this.loadNotes();
  }

  async loadNotes() {
    this.notes = [];
    const notesData = await this.noteService.getNotes();
    notesData.forEach((doc) => {
      const data = doc.data();
      this.notes.push({
        id: doc.id,
        title: data['title'],
        content: data['content'],
      });
    });
  }

  newNote() {
    this.router.navigateByUrl('/detail/new');
  }

  onDelete(event: any) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this note?',
      header: 'Confirmation',
      accept: async () => {
        await this.noteService.deleteNote(event.id);
        this.loadNotes();
      },
      reject: () => {},
    });
  }
}
