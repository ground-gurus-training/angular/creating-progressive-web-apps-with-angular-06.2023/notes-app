import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { HomeComponent } from './home.component';
import { NoteComponent } from 'src/app/components/note/note.component';
import { HeaderComponent } from 'src/app/layouts/header/header.component';
import { FooterComponent } from 'src/app/layouts/footer/footer.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    NoteComponent,
    HeaderComponent,
    FooterComponent,
    ButtonModule,
    ConfirmDialogModule,
    CommonModule,
  ],
  exports: [HomeComponent],
})
export class HomeModule {}
