import { Injectable } from '@angular/core';
import {
  Firestore,
  addDoc,
  collection,
  deleteDoc,
  doc,
  getDoc,
  getDocs,
  updateDoc,
} from '@angular/fire/firestore';
import { Note } from '../model/note';

@Injectable({
  providedIn: 'root',
})
export class NoteService {
  constructor(private firestore: Firestore) {}

  getNotes() {
    const notesCollection = collection(this.firestore, 'notes');
    return getDocs(notesCollection);
  }

  getNote(id: string) {
    const ref = doc(this.firestore, `notes/${id}`);
    return getDoc(ref);
  }

  addNote(data: any) {
    const ref = collection(this.firestore, `notes`);
    return addDoc(ref, this.setUndefinedValuesToNull(data));
  }

  saveNote(note: Note) {
    const ref = doc(this.firestore, `notes/${note.id}`);
    return updateDoc(ref, this.setUndefinedValuesToNull(note));
  }

  deleteNote(id: string) {
    const ref = doc(this.firestore, `notes/${id}`);
    return deleteDoc(ref);
  }

  setUndefinedValuesToNull(data: any) {
    Object.keys(data)
      .filter((k) => data[k] == undefined)
      .forEach((k) => (data[k] = null));
    return data;
  }
}
