import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';

import { ButtonModule } from 'primeng/button';
import { ConfirmPopupModule } from 'primeng/confirmpopup';

import { Note } from 'src/app/model/note';

@Component({
  standalone: true,
  imports: [ButtonModule, ConfirmPopupModule],
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent {
  @Input({ required: true }) note?: Note;

  @Output() deleted: EventEmitter<any> = new EventEmitter();

  constructor(private messageService: MessageService, private router: Router) {}

  edit() {
    this.router.navigateByUrl(`/detail/${this.note?.id}`);
  }

  delete() {
    if (this.note) {
      this.deleted.emit({
        id: this.note.id,
        event: 'delete',
      });
    }
  }
}
